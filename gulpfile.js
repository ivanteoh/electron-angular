var gulp = require('gulp'),
    del = require('del'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    webserver = require('gulp-webserver'),
    electron = require('gulp-atom-electron'),
    symdest = require('gulp-symdest');

var config = {
    sourceDir:   'src',
    buildDir:    'build',
    packagesDir: 'packages',
    npmDir:      'node_modules',
    bowerDir:    'bower_components'
};

// run clean tasks
gulp.task('clean', [
  'clean:build',
  'clean:package'
]);

gulp.task('clean:build', function() {
  return del(config.buildDir + '/**/*', { force: true });
});

gulp.task('clean:package', function() {
  return del(config.packagesDir + '/**/*', { force: true });
});

// run development task
gulp.task('dev', [
  'dev:watch',
  'dev:serve'
]);

// serve the build dir
gulp.task('dev:serve', function () {
  gulp.src(config.buildDir)
    .pipe(webserver({
      open: true
    }));
});

// watch for changes and run the relevant task
gulp.task('dev:watch', function() {
  gulp.watch(config.sourceDir + '/**/*.js',   ['frontend:js']);
  gulp.watch(config.sourceDir + '/**/*.html', ['frontend:html']);
  gulp.watch(config.sourceDir + '/**/*.css',  ['frontend:css']);
});

gulp.task('frontend', [
  'frontend:dependencies',
  'frontend:js',
  'frontend:html',
  'frontend:css'
]);

// move dependencies into build dir
gulp.task('frontend:dependencies', function() {
  return gulp.src([
    config.npmDir + '/angular/angular.js',
    config.npmDir + '/material-design-lite/material.js',
    config.npmDir + '/material-design-lite/material.css',
    config.npmDir + '/angular-route/angular-route.js'
  ])
    .pipe(gulp.dest(config.buildDir + '/lib'));
});

// transpile & move js
gulp.task('frontend:js', function() {
  return gulp.src(config.sourceDir + '/**/*.js')
    .pipe(gulp.dest(config.buildDir));
});

// move html
gulp.task('frontend:html', function () {
  return gulp.src(config.sourceDir + '/**/*.html')
    .pipe(gulp.dest(config.buildDir));
});

// move css
gulp.task('frontend:css', function () {
  return gulp.src(config.sourceDir + '/**/*.css')
    .pipe(gulp.dest(config.buildDir));
});

// electron app
gulp.task('electron', function() {
  return gulp.src([
    config.sourceDir + '/electron/main.js',
    config.sourceDir + '/electron/package.json'
  ])
    .pipe(gulp.dest(config.buildDir));
});

// package electron
gulp.task('package', [
  'package:osx',
  'package:linux',
  'package:windows'
]);

gulp.task('package:osx', function() {
  return gulp.src(config.buildDir + '/**/*')
    .pipe(electron({
      version: '0.36.7',
      platform: 'darwin'
    }))
    .pipe(symdest(config.packagesDir + '/osx'));
});

gulp.task('package:linux', function() {
  return gulp.src(config.buildDir + '/**/*')
    .pipe(electron({
      version: '0.36.7',
      platform: 'linux'
    }))
    .pipe(symdest(config.packagesDir + '/linux'));
});

gulp.task('package:windows', function() {
  return gulp.src(config.buildDir + '/**/*')
    .pipe(electron({
      version: '0.36.7',
      platform: 'win32'
    }))
    .pipe(symdest(config.packagesDir + '/windows'));
});
