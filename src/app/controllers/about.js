'use strict';

/**
 * @ngdoc function
 * @name yoMytodoApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the yoMytodoApp
 */
angular.module('myApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
